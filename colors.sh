#!/bin/bash
cat << EOF > colors.adoc
= Julakan karan lon o lon !
:revealjs_theme: beige 
:title-slide-transition: zoom
:revealjs_transition: convex
//:revealjs_slidenumber: true
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: true
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:customcss: custom.css
:imagesdir: ./images
:docinfo: shared
:docinfo: shared
:icons: font

Trouvez le mot jula corréspondant à la couleur présentée. +
La flêche droite affiche une couleur, et ensuite le nom de la couleur en jula. +

====
**I ni baara !**
====

//red
[background-color="#D82E3F"]
== Nin ye ɲa jumɛn ye ?

[%step]
Ɲa ye wulenman ye 

//yellow
[background-color="#FFE135"]
== Nin ye ɲa jumɛn ye ?

[%step]
Ɲa ye nɛrɛmugulaman ye 

//blue
[background-color="#3581D8"]
== Nin ye ɲa jumɛn ye ?

[%step]
Ɲa ye bulalaman ye 

//green
[background-color="#28CC2D"]
== Nin ye ɲa jumɛn ye ?

[%step]
Ɲa ye binkɛnɛlaman ye 

//black
[background-color="#333333"]
== Nin ye ɲa jumɛn ye ?

[%step]
Ɲa ye finman ye 

//white (cream)
[background-color="beige", .color.grey]
== Nin ye ɲa jumɛn ye ?

[%step]
Ɲa ye gbɛman ye 

//multicolored
[background-color="#333333"]
== Nin ye ɲa jumɛn ye ?

image::oiseau-75.jpg[background, size="75%"]

[.step]#kɔnɔnin# [.step]#ɲɛgɛɲɛgɛlen# 

//spotted
[background-color="#333333"]
== Nin ye ɲa jumɛn ye ?

image::basa-75.jpg[role="left"]

[.step]#basa# [.step]#tɔmitɔmilen#  

//multicolored
[background-color="#333333"]
== Nin ye ɲa jumɛn ye ?

image::velos-75.jpg[background, size="contain"]

[.step]#nɛgɛso# [.step]#ɲɛgɛɲɛgɛlen# 

[transition="zoom"]
== Félicitations ! !

Mais n'oubliez pas d'apprendre aussi +
les https://slides-dyu.coastsystems.net/index.html[cinq phrases du jour]! 

EOF
