#!/bin/bash
in=Money.csv
dest=./
quiz=money.adoc
theme=black
source ./params.sh
#[[ -f ~/Documents/Jula/brainbrew/Jula/src/data/Money.csv ]] && cp -v ~/Documents/Jula/brainbrew/Jula/src/data/Money.csv .

for x in gawk shuf csvtool; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp  "./theme/$theme.css" node_modules/reveal.js/css/theme/
cp  ./theme/*.css node_modules/reveal.js/css/theme/
#cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/
#cp ./images/cfa* node_modules/reveal.js/css/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

cat <<EOF > $quiz 
= Wariko karan lon o lon !
:revealjs_theme: $theme
:source-highlighter: highlight.js
:title-slide-transition: zoom
:title-slide-background-image: cfa.orig.jpg
:title-slide-background-size: 100% 
:title-slide-background-opacity: .8
:revealjs_transition: concave 
//:revealjs_slidenumber: true
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:customcss: custom.css
:imagesdir: ./images
:docinfo: shared
:icons: font

[.r-fit-text]
Pour un étranger, l'argent en Jula n'est pas facile !
Venez chaque jour pour devenir habile au marché avec les 'darasi!' 
La flêche droite affiche un montant en F CFA et ensuite en Jula.

====
**I ni lɔgɔ !**
====

image::cfa.orig.jpg[background, size=cover]

EOF

csvtool -t COMMA -u TAB cat $in | grep argent | \
shuf -n 5 | awk -v quiz=$quiz -F "\t" '
{ alt=srand() }
{ if (!($6 == ""))
  {
  { print "[%notitle, background-color='#333333']" >> quiz };
  { print "== ", $2, "\n\n" >> quiz};
  { image = gensub(/(.*)(")(.*\.png)(.*)/, "\\3", "g", $6); 
    print "image::"image"[background, size=cover]\n\n">> quiz 
  };
  { print "[.r-fit-text,step=1]" >> quiz };
  { print "**"$5"**", "\n\n" >> quiz};
}
else {
  { print "[background-color='#28cc2d']" >> quiz };
  { print "== ", $2, "\n\n" >> quiz};
  { print "[.r-fit-text,step=1]" >> quiz };
  { print "**"$5"**", "\n\n" >> quiz};
    }
  }'

csvtool -t COMMA -u TAB cat $in | grep argent | \
  shuf -n 5 | awk -v quiz=$quiz -F "\t" '
  { alt=srand() }
  { print "[background-color='#3581D8']" >> quiz }
  { print "== ", $5, "\n" >> quiz}
  { print "[.r-fit-text,step=1]" >> quiz }
  { print "**"$2"**", "\n\n" >> quiz}
  { if (!($6 == ""))
    { image = gensub(/(.*)(")(.*\.png)(.*)/, "\\3", "g", $6);
      print "image::"image"[step=1]\n\n">> quiz }
    }'

  printf "[background-color="#333333"]\n" >> $quiz
  printf "== Félicitations ! !\n\n" >> $quiz
  printf "Mais n'oubliez pas de revenir demain pour un nouveau séance de 'Wari'.\n\n" >> $quiz

  printf "image::cfa.orig.jpg[background, size=cover, opacity=.5]\n\n" >> $quiz

#  npx asciidoctor-revealjs -D $dest  *.adoc 

