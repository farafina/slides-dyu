#!/bin/bash
in=headwords-6.tsv
dest=./
quiz=index.adoc
tsv="quiz.tsv"
theme=julakan
export days
export months
export dows
source ./params.sh

for x in shuf; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp  "./theme/$theme.css" node_modules/reveal.js/css/theme/
cp  ./theme/*.css node_modules/reveal.js/css/theme/
#cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/
#cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

cat <<EOF > $quiz 
= Julakan karan lon o lon !
:revealjs_theme: $theme
:source-highlighter: highlight.js
:title-slide-transition: zoom
//:revealjs_slidenumber: true
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:customcss: custom.css
:imagesdir: ./images
:docinfo: shared
:icons: font

Trouvez la traduction exacte des phrases jula suivantes vers le français. +
La flêche droite affiche d'abord un indice, et ensuite la traduction en français.
Il y a du nouveau chaque jour, alors revenez demain! +

====
**I ni baara !**
====

EOF

printf "== Bi ye lon jumɛn ye ?\n" >> $quiz
printf "[step=1]\n" >> $quiz
printf "bi ye [.step]#%s# ye,\n\n" "$dow" >> $quiz
echo -e '[%step]\n' >> $quiz
echo -e "[%step]#$month#, tere [.step]#$day#,\n\n" >> $quiz
echo -e "[%step]\n" >> $quiz
printf "saan [.step]#waga fila ani mugan ni kelen kɔnɔ#\n\n" >> $quiz

shuf -n 5 $in | awk -v quiz=$quiz -F"\t" '{ hint=$2 }{ print "== ", $3, "\n" >> quiz}
{ print "[TIP,step=1]" >> quiz }
{ print hint, "\n" >> quiz}
{ print "[.r-fit-text,step=2]" >> quiz }
{ print "**"$4"**", "\n\n" >> quiz}'

printf "== Félicitations ! !\n\n" >> $quiz
printf "Mais n'oubliez pas de revenir demain pour un nouveau jeu de phrases.\n" >> $quiz

#npx asciidoctor-revealjs -v -D $dest  *.adoc 

