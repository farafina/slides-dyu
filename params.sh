#!/usr/bin/bash
dows=(
  "tenenlon"
  "taratalon"
  "arabalon"
  "alamisalon"
  "jumalon"
  "sibirilon"
  "karilon"
)

months=(
  "zanwiyekalo"
  "feburukalo"
  "marisikalo"
  "awirilikalo"
  "mɛkalo"
  "zuɛnkalo"
  "zuwekalo"
  "utikalo"
  "setanburukalo"
  "ɔkutɔburukalo"
  "novanburukalo"
  "desanburukalo"
)

days=(
  "kelen"
  "fila"
  "saba"
  "naani"
  "looru"
  "wɔrɔ"
  "woronfila"
  "seegi"
  "kɔnɔtɔ"
  "tan"
  "tan ni kelen"
  "tan ni fila"
  "tan ni saba"
  "tan ni naani"
  "tan ni looru"
  "tan ni wɔrɔ"
  "tan ni woronfila"
  "tan ni seegi"
  "tan ni kɔnɔtɔ"
  "mugan"
  "mugan ni kelen"
  "mugan ni fila"
  "mugan ni saba"
  "mugan ni naani"
  "mugan ni looru"
  "mugan ni wɔrɔ"
  "mugan ni woronfila"
  "mugan ni seegi"
  "mugan ni kɔnɔtɔ"
  "bi saba"
  "bi saba ni kelen"
  )

export day=${days[$(date +%-d)-1]}
export dow=${dows[$(date +%-w)-1]}
export month=${months[$(date +%-m)-1]}

declare -A red
red[color]=red
red[jula]=wuleman

declare -A yellow
yellow[color]=yellow
yellow[jula]=nereman

declare -A black
black[color]=black
black[jula]=finman

